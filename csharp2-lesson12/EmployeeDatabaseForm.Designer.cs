﻿namespace csharp2_lesson12
{
    partial class EmployeeDatabaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.rmbutton = new System.Windows.Forms.Button();
            this.employeescountvaluelabel = new System.Windows.Forms.Label();
            this.employeeslistbox = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(23, 26);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Add";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // rmbutton
            // 
            this.rmbutton.Location = new System.Drawing.Point(23, 89);
            this.rmbutton.Name = "rmbutton";
            this.rmbutton.Size = new System.Drawing.Size(75, 23);
            this.rmbutton.TabIndex = 25;
            this.rmbutton.Text = "Remove";
            this.rmbutton.UseVisualStyleBackColor = true;
            this.rmbutton.Click += new System.EventHandler(this.rmbutton_Click);
            // 
            // employeescountvaluelabel
            // 
            this.employeescountvaluelabel.AutoSize = true;
            this.employeescountvaluelabel.Location = new System.Drawing.Point(37, 160);
            this.employeescountvaluelabel.Name = "employeescountvaluelabel";
            this.employeescountvaluelabel.Size = new System.Drawing.Size(35, 13);
            this.employeescountvaluelabel.TabIndex = 24;
            this.employeescountvaluelabel.Text = "Count";
            // 
            // employeeslistbox
            // 
            this.employeeslistbox.FormattingEnabled = true;
            this.employeeslistbox.Location = new System.Drawing.Point(126, 26);
            this.employeeslistbox.Name = "employeeslistbox";
            this.employeeslistbox.Size = new System.Drawing.Size(307, 238);
            this.employeeslistbox.TabIndex = 23;
            this.employeeslistbox.SelectedIndexChanged += new System.EventHandler(this.employeeslistbox_SelectedIndexChanged);
            // 
            // EmployeeDatabaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 292);
            this.Controls.Add(this.rmbutton);
            this.Controls.Add(this.employeescountvaluelabel);
            this.Controls.Add(this.employeeslistbox);
            this.Controls.Add(this.button1);
            this.Name = "EmployeeDatabaseForm";
            this.Text = "Employee Database";
            this.Load += new System.EventHandler(this.EmployeeDatabaseForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button rmbutton;
        private System.Windows.Forms.Label employeescountvaluelabel;
        private System.Windows.Forms.ListBox employeeslistbox;
    }
}

