﻿namespace csharp2_lesson12
{
    partial class EmployeeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.idLabel = new System.Windows.Forms.Label();
            this.idbox = new System.Windows.Forms.TextBox();
            this.add1box = new System.Windows.Forms.TextBox();
            this.add2box = new System.Windows.Forms.TextBox();
            this.citybox = new System.Windows.Forms.TextBox();
            this.statebox = new System.Windows.Forms.TextBox();
            this.okbutton = new System.Windows.Forms.Button();
            this.cancelbutton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.zipbox = new System.Windows.Forms.TextBox();
            this.pnbox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.fnbox = new System.Windows.Forms.TextBox();
            this.lnbox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // idLabel
            // 
            this.idLabel.AutoSize = true;
            this.idLabel.Location = new System.Drawing.Point(35, 126);
            this.idLabel.Name = "idLabel";
            this.idLabel.Size = new System.Drawing.Size(15, 13);
            this.idLabel.TabIndex = 0;
            this.idLabel.Text = "id";
            // 
            // idbox
            // 
            this.idbox.Location = new System.Drawing.Point(119, 123);
            this.idbox.Name = "idbox";
            this.idbox.Size = new System.Drawing.Size(100, 20);
            this.idbox.TabIndex = 1;
            this.idbox.Text = "pending";
            // 
            // add1box
            // 
            this.add1box.Location = new System.Drawing.Point(119, 160);
            this.add1box.Name = "add1box";
            this.add1box.Size = new System.Drawing.Size(100, 20);
            this.add1box.TabIndex = 2;
            // 
            // add2box
            // 
            this.add2box.Location = new System.Drawing.Point(119, 197);
            this.add2box.Name = "add2box";
            this.add2box.Size = new System.Drawing.Size(100, 20);
            this.add2box.TabIndex = 3;
            // 
            // citybox
            // 
            this.citybox.Location = new System.Drawing.Point(119, 226);
            this.citybox.Name = "citybox";
            this.citybox.Size = new System.Drawing.Size(100, 20);
            this.citybox.TabIndex = 4;
            // 
            // statebox
            // 
            this.statebox.Location = new System.Drawing.Point(119, 260);
            this.statebox.Name = "statebox";
            this.statebox.Size = new System.Drawing.Size(100, 20);
            this.statebox.TabIndex = 5;
            // 
            // okbutton
            // 
            this.okbutton.Location = new System.Drawing.Point(32, 439);
            this.okbutton.Name = "okbutton";
            this.okbutton.Size = new System.Drawing.Size(75, 23);
            this.okbutton.TabIndex = 6;
            this.okbutton.Text = "OK";
            this.okbutton.UseVisualStyleBackColor = true;
            this.okbutton.Click += new System.EventHandler(this.okbutton_Click);
            // 
            // cancelbutton
            // 
            this.cancelbutton.Location = new System.Drawing.Point(144, 439);
            this.cancelbutton.Name = "cancelbutton";
            this.cancelbutton.Size = new System.Drawing.Size(75, 23);
            this.cancelbutton.TabIndex = 7;
            this.cancelbutton.Text = "Cancel";
            this.cancelbutton.UseVisualStyleBackColor = true;
            this.cancelbutton.Click += new System.EventHandler(this.cancelbutton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 167);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "address 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 200);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "address 2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 233);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "city";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(35, 267);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "state";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 310);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(20, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "zip";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(32, 351);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "phone number";
            // 
            // zipbox
            // 
            this.zipbox.Location = new System.Drawing.Point(119, 310);
            this.zipbox.Name = "zipbox";
            this.zipbox.Size = new System.Drawing.Size(100, 20);
            this.zipbox.TabIndex = 14;
            // 
            // pnbox
            // 
            this.pnbox.Location = new System.Drawing.Point(119, 351);
            this.pnbox.Name = "pnbox";
            this.pnbox.Size = new System.Drawing.Size(100, 20);
            this.pnbox.TabIndex = 15;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(35, 53);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "first name";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(35, 93);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "last name";
            // 
            // fnbox
            // 
            this.fnbox.Location = new System.Drawing.Point(119, 45);
            this.fnbox.Name = "fnbox";
            this.fnbox.Size = new System.Drawing.Size(100, 20);
            this.fnbox.TabIndex = 20;
            // 
            // lnbox
            // 
            this.lnbox.Location = new System.Drawing.Point(119, 85);
            this.lnbox.Name = "lnbox";
            this.lnbox.Size = new System.Drawing.Size(100, 20);
            this.lnbox.TabIndex = 21;
            // 
            // EmployeeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(287, 511);
            this.Controls.Add(this.lnbox);
            this.Controls.Add(this.fnbox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.pnbox);
            this.Controls.Add(this.zipbox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cancelbutton);
            this.Controls.Add(this.okbutton);
            this.Controls.Add(this.statebox);
            this.Controls.Add(this.citybox);
            this.Controls.Add(this.add2box);
            this.Controls.Add(this.add1box);
            this.Controls.Add(this.idbox);
            this.Controls.Add(this.idLabel);
            this.Name = "EmployeeForm";
            this.Text = "EmployeeForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label idLabel;
        private System.Windows.Forms.TextBox idbox;
        private System.Windows.Forms.TextBox add1box;
        private System.Windows.Forms.TextBox add2box;
        private System.Windows.Forms.TextBox citybox;
        private System.Windows.Forms.TextBox statebox;
        private System.Windows.Forms.Button okbutton;
        private System.Windows.Forms.Button cancelbutton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox zipbox;
        private System.Windows.Forms.TextBox pnbox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox fnbox;
        private System.Windows.Forms.TextBox lnbox;
    }
}