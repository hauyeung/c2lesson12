﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace csharp2_lesson12
{

    public class employee
    {
        public int id;
        public string fn;
        public string ln;
        public string add1;
        public string add2;
        public string city;
        public string state;
        public int zip;
        public string pn;
        public employee(int id, string fn, string ln, string add1, string add2, string city, string state, int zip, string pn)
        {
            this.id = id;
            this.fn = fn;
            this.ln = ln;
            this.add1 = add1;
            this.add2 = add2;
            this.city = city;
            this.state = state;
            this.zip = zip;
            this.pn = pn;
        }

        
    }
}
