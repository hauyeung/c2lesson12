﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace csharp2_lesson12
{
    public partial class EmployeeDatabaseForm : Form
    {
        static int num = 0;
        private ArrayList employees = new ArrayList();
        public EmployeeDatabaseForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            using (EmployeeForm ef = new EmployeeForm())
            {
                if (ef.ShowDialog() == DialogResult.OK)
                {
                    employee emp = ef.EmployeeInfo;
                    employees.Add(emp);
                    string entry = emp.id + " " + emp.fn + " " + emp.ln;
                    this.employeeslistbox.Items.Add(entry);
                    num++;
                    employeescountvaluelabel.Text = Convert.ToString(num);
                }
            }
            
            
        }

        private void rmbutton_Click(object sender, EventArgs e)
        {
            DialogResult d = MessageBox.Show("Are you sure you want to delete the entry?", "Delete Employee", MessageBoxButtons.YesNo);
            if (d == DialogResult.Yes)
            {
                employeeslistbox.Items.Remove(employeeslistbox.SelectedItem);
                num--;
                employeescountvaluelabel.Text = Convert.ToString(num);
            }
        }

        private void EmployeeDatabaseForm_Load(object sender, EventArgs e)
        {
            rmbutton.Enabled = false;
        }

        private void employeeslistbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            rmbutton.Enabled = true;
        }
    }
}
