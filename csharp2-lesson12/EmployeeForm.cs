﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace csharp2_lesson12
{
    public partial class EmployeeForm : Form
    {
        private static int id = 0;
        private employee _employeeInfo;
        public employee EmployeeInfo
        {
            get
            {
                return this._employeeInfo;
            }
        }
 
        public EmployeeForm()
        {
            InitializeComponent();
        }

   

        private void cancelbutton_Click(object sender, EventArgs e)
        {
            Close();
        }



        private void okbutton_Click(object sender, EventArgs e)
        {

            string fn = fnbox.Text;
            string ln = lnbox.Text;
            string add1 = add1box.Text;
            string add2 = add2box.Text;
            string city = citybox.Text;
            string state = statebox.Text;
            int zip = Convert.ToInt32(zipbox.Text);
            string phone = pnbox.Text;
            _employeeInfo = new employee(id, fn, ln, add1, add2, city, state, zip, phone);
            id++;                  
            this.DialogResult = DialogResult.OK;
           
        }
    
    }
}
